/*
Un cartero debe repartir un conjunto de piezas de correspondencia que le entrega el correo.
Cada vez que el cartero recibe todas las piezas las debe registrar en un software 
para poder llevar un control de las mismas durante su recorrido.

Para eso nos pidió un software donde pueda cargar por teclado todas las piezas 
que le entregaron para informar el resultado al final del recorrido del día.

De cada pieza sabe el número de la misma, su tipo (1 para cartas simples, 2 para 
paquetes y 3 para sobres con vencimiento), el peso (en gramos) y un número que 
indica con 1 si la pudo entregar correctamente y 0 en caso contrario.

Luego de la carga el software debe informar:

*Cantidad de piezas sin entregar
*Sumatoria del peso de todas las piezas de tipo 2
*Dado un número de pieza indicado por el usuario, informar si la pudo entregar o no.
 */
package parcialmacapecha;

/**
 *
 * @author Maka
 */
public class Correspondencia {
    private Pieza [] pieza;

    public Correspondencia() {
    }
    
    public Correspondencia(int cantidadPiezas) {
        this.pieza = new Pieza[cantidadPiezas];
    }
    
    public void agregarPiezas(Pieza pieza){
        for (int i = 0; i < this.pieza.length; i++) {
            if (this.pieza[i] == null) {
                this.pieza[i] = pieza;
                break;
            }
        }
    }
    
    public int cantidadPiezasSinEntregar() {
        int cantidad = 0;
        for (int i = 0; i < this.pieza.length; i++) {
            if (this.pieza[i] != null && this.pieza[i].getEntrego() == 0) {
                cantidad ++;
            }
        }
        return cantidad;
    }
    
    public float sumaPesoPiezasTipo2() {
        int suma = 0;
        for (int i = 0; i < this.pieza.length; i++) {
            if (this.pieza[i] != null && this.pieza[i].getTipo() == 2) {
                suma+= this.pieza[i].getPeso();
            }
        }
        return suma;
    }
    
    public void seEntrego(int numero){
        String texto = "Esta pieza no se encontró";
        for (int i = 0; i < this.pieza.length; i++) {
            if (this.pieza[i] != null && this.pieza[i].getEntrego() == 1 && this.pieza[i].getNumero() == numero) {
                texto = "La pieza ha sido entregada";
                break;
            } else if (this.pieza[i].getNumero() == numero) {
                texto = "La pieza no se entregó";
            }
        }
        System.out.println(texto);
    }
   
}
