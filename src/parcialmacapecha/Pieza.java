/*
Un cartero debe repartir un conjunto de piezas de correspondencia que le entrega el correo.
Cada vez que el cartero recibe todas las piezas las debe registrar en un software 
para poder llevar un control de las mismas durante su recorrido.

Para eso nos pidió un software donde pueda cargar por teclado todas las piezas 
que le entregaron para informar el resultado al final del recorrido del día.

De cada pieza sabe el número de la misma, su tipo (1 para cartas simples, 2 para 
paquetes y 3 para sobres con vencimiento), el peso (en gramos) y un número que 
indica con 1 si la pudo entregar correctamente y 0 en caso contrario.

Luego de la carga el software debe informar:

*Cantidad de piezas sin entregar
*Sumatoria del peso de todas las piezas de tipo 2
*Dado un número de pieza indicado por el usuario, informar si la pudo entregar o no.
 */
package parcialmacapecha;

/**
 *
 * @author Maka
 */
public class Pieza {
    private int numero;
    private int tipo; // 1- cartas simples, 2- paquetes, 3- sobres con vencimiento
    private float peso;
    private int entrego; // 1- si, 0- no

    public Pieza() {
    }

    public Pieza(int numero, int tipo, float peso, int entrego) {
        this.numero = numero;
        this.tipo = tipo;
        this.peso = peso;
        this.entrego = entrego;
    }

    public int getNumero() {
        return numero;
    }

    public int getTipo() {
        return tipo;
    }

    public float getPeso() {
        return peso;
    }

    public int getEntrego() {
        return entrego;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setEntrego(int entrego) {
        this.entrego = entrego;
    }

    @Override
    public String toString() {
        return "Pieza{" + "numero=" + numero + ", tipo=" + tipo + ", peso=" + peso + ", entrego=" + entrego + '}';
    }
    
    
}
