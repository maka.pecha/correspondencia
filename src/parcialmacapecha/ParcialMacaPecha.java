/*
Un cartero debe repartir un conjunto de piezas de correspondencia que le entrega el correo.
Cada vez que el cartero recibe todas las piezas las debe registrar en un software 
para poder llevar un control de las mismas durante su recorrido.

Para eso nos pidió un software donde pueda cargar por teclado todas las piezas 
que le entregaron para informar el resultado al final del recorrido del día.

De cada pieza sabe el número de la misma, su tipo (1 para cartas simples, 2 para 
paquetes y 3 para sobres con vencimiento), el peso (en gramos) y un número que 
indica con 1 si la pudo entregar correctamente y 0 en caso contrario.

Luego de la carga el software debe informar:

*Cantidad de piezas sin entregar
*Sumatoria del peso de todas las piezas de tipo 2
*Dado un número de pieza indicado por el usuario, informar si la pudo entregar o no.
 */
package parcialmacapecha;
import java.util.Scanner;

/**
 *
 * @author Maka
 */
public class ParcialMacaPecha {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("PROGRAMA DE CORRESPONDENCIA: \n");
        int cantidadPiezas = 0;
        
        boolean checkCantidad = true;
        while (checkCantidad) {
            System.out.println("Ingrese Cantidad de Piezas: ");
            String input = sc.nextLine();
            try {
              int x = Integer.parseInt(input);
              cantidadPiezas = x;
              checkCantidad = false;
            }catch(NumberFormatException e) {
              System.out.println("Debe ser de tipo número!"); 
            } 
        }
        
        Correspondencia c = new Correspondencia (cantidadPiezas);
        System.out.println("INGRESE CADA UNA DE " + cantidadPiezas + " PIEZAS");
        
        for (int i = 0; i < cantidadPiezas; i++) {
            int numero = -1;
            int tipo = -1;
            float peso = -1;
            int entrego = -1;
            
            boolean checkNumero = true;
            while (checkNumero) {
                System.out.println("Ingrese el número de la pieza " + (i+1));
                String input = sc.nextLine();
                try {
                  int x = Integer.parseInt(input);
                  numero = x;
                  checkNumero = false;
                }catch(NumberFormatException e) {
                  System.out.println("Debe ser de tipo número!"); 
                } 
            }
            
            boolean checkTipo = true;
            while (checkTipo) {
                System.out.println("Ingrese el Tipo de la pieza "+ (i+1)+ ", siendo \"1\" para Cartas simples, \"2\" para Paquetes o \"3\" para Sobres con vencimiento");
                String input = sc.nextLine();
                try {
                  int x = Integer.parseInt(input);
                  if(x == 1|| x == 2 || x==3) {
                    tipo = x;
                    checkTipo = false;
                  } else {
                      System.out.println("Debe ingresar \"1\" para Cartas simples, \"2\" para Paquetes o \"3\" para Sobres con vencimiento!");
                  }
                }catch(NumberFormatException e) {
                  System.out.println("Debe ser de tipo número!"); 
                } 
            }

            boolean checkPeso = true;
            while (checkPeso) {
                System.out.println("Ingrese el Peso en Gramos de la pieza " + (i+1) + ": ");
                String input = sc.nextLine();
                try {
                  float x = Float.parseFloat(input);
                  peso = x;
                  checkPeso = false;
                }catch(NumberFormatException e) {
                  System.out.println("Debe ser un número de tipo Float!"); 
                } 
            }
            
            boolean checkEntrego = true;
            while (checkEntrego) {
                System.out.println("Ingrese si se entregó la pieza " + (i+1) + ", siendo \"1\" para SI, o \"0\" para NO");
                String input = sc.nextLine();
                try {
                  int x = Integer.parseInt(input);
                  if(x == 0 || x == 1) {
                    entrego = x;
                    checkEntrego = false;
                  } else {
                      System.out.println("Debe ingresar \"1\" para SI, o \"0\" para NO!");
                  }
                }catch(NumberFormatException e) {
                  System.out.println("Debe ser de tipo número!"); 
                } 
            }
            
            Pieza p = new Pieza(numero, tipo, peso, entrego);
            c.agregarPiezas(p);
        }
        
        System.out.println("INFORMACION CORRESPONDENCIA DEL DIA");
        System.out.println("Cantidad de piezas sin entregar: " + c.cantidadPiezasSinEntregar());
        System.out.println("Sumatoria del peso de todas las piezas de tipo 2: " + c.sumaPesoPiezasTipo2());

        int pieza = -1;
        boolean checkPieza = true;
        boolean consultarOtraVez = true;
        while(consultarOtraVez){
            while (checkPieza) {
                System.out.println("Ingrese Número de Pieza para saber si ha sido entregada: ");
                String input = sc.nextLine();
                try {
                  int x = Integer.parseInt(input);
                  pieza = x;
                  c.seEntrego(pieza);
                  checkPieza = false;
                }catch(NumberFormatException e) {
                  System.out.println("Debe ser de tipo número!"); 
                } 
            }
            System.out.println("Desea hacer otra consulta? (ingrese \"s\" para SI y \"n\" para NO)");
            String userInput = sc.next().toLowerCase();
            switch (userInput) {
                case "s":
                    checkPieza = true;
                    sc.nextLine();
                    break ;
                case "n":
                    checkPieza = false;
                    consultarOtraVez = false;
                    break ;
                default:
                    System.out.println("Responda S o N");
                    break;
            }
        }
        System.out.println("---FIN DEL PROGRAMA---");
    }
    
}
